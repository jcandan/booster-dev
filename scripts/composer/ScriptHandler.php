<?php

/**
 * @file
 * Contains \BoosterDev\composer\ScriptHandler.
 *
 * A modified copy of the script handler from ThunderDevelop, which is a
 * modified copy of the DrupalProject ScriptHandler.
 *
 * @see DrupalProject\composer\ScriptHandler
 * @see ThunderDevelop\composer\ScriptHandler
 */

namespace BoosterDev\composer;

use Composer\Composer;
use Composer\Package\PackageInterface;
use Composer\Script\Event;
use Composer\Semver\Comparator;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class ScriptHandler {

  public static function createRequiredFiles(Event $event) {
    $fs = new Filesystem();
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();
    $dirs = [
      'modules',
      'profiles',
      'themes',
    ];
    // Required for unit testing
    foreach ($dirs as $dir) {
      if (!$fs->exists($drupalRoot . '/' . $dir)) {
        $fs->mkdir($drupalRoot . '/' . $dir);
        $fs->touch($drupalRoot . '/' . $dir . '/.gitkeep');
      }
    }
    // Prepare the settings file for installation
    if (!$fs->exists($drupalRoot . '/sites/default/settings.php') and $fs->exists($drupalRoot . '/sites/default/default.settings.php')) {
      $fs->copy($drupalRoot . '/sites/default/default.settings.php', $drupalRoot . '/sites/default/settings.php');
      require_once $drupalRoot . '/core/includes/bootstrap.inc';
      require_once $drupalRoot . '/core/includes/install.inc';
      $settings['config_directories'] = [
        CONFIG_SYNC_DIRECTORY => (object) [
          'value' => Path::makeRelative($drupalFinder->getComposerRoot() . '/config/sync', $drupalRoot),
          'required' => TRUE,
        ],
      ];
      drupal_rewrite_settings($settings, $drupalRoot . '/sites/default/settings.php');
      $fs->chmod($drupalRoot . '/sites/default/settings.php', 0666);
      $event->getIO()->write("Created a sites/default/settings.php file with chmod 0666");
    }
    // Create the files directory with chmod 0777
    if (!$fs->exists($drupalRoot . '/sites/default/files')) {
      $oldmask = umask(0);
      $fs->mkdir($drupalRoot . '/sites/default/files', 0777);
      umask($oldmask);
      $event->getIO()->write("Created a sites/default/files directory with chmod 0777");
    }
  }

  /**
   * Checks if the installed version of Composer is compatible.
   *
   * Composer 1.0.0 and higher consider a `composer install` without having a
   * lock file present as equal to `composer update`. We do not ship with a lock
   * file to avoid merge conflicts downstream, meaning that if a project is
   * installed with an older version of Composer the scaffolding of Drupal will
   * not be triggered. We check this here instead of in drupal-scaffold to be
   * able to give immediate feedback to the end user, rather than failing the
   * installation after going through the lengthy process of compiling and
   * downloading the Composer dependencies.
   *
   * @see https://github.com/composer/composer/pull/5035
   */
  public static function checkComposerVersion(Event $event) {
    $composer = $event->getComposer();
    $io = $event->getIO();
    $version = $composer::VERSION;
    // The dev-channel of composer uses the git revision as version number,
    // try to the branch alias instead.
    if (preg_match('/^[0-9a-f]{40}$/i', $version)) {
      $version = $composer::BRANCH_ALIAS_VERSION;
    }
    // If Composer is installed through git we have no easy way to determine if
    // it is new enough, just display a warning.
    if ($version === '@package_version@' || $version === '@package_branch_alias_version@') {
      $io->writeError('<warning>You are running a development version of Composer. If you experience problems, please update Composer to the latest stable version.</warning>');
    }
    elseif (Comparator::lessThan($version, '1.0.0')) {
      $io->writeError('<error>Drupal-project requires Composer version 1.0.0 or higher. Please update your Composer before continuing</error>.');
      exit(1);
    }
  }

  /**
   * Clone specified projects as git-submodules.
   *
   * Projects to clone are specified in the local-develop-packages directive.
   * It is expected you have clone access permissions to the specified
   * repository.
   *
   * @param \Composer\Script\Event $event
   */
  public static function downloadDevelopPackages(Event $event) {
    $fs = new Filesystem();

    $io = $event->getIO();
    $composer = $event->getComposer();
    $repositoryManager = $composer->getRepositoryManager();
    $rootPackage = $composer->getPackage();

    $rootExtra = $rootPackage->getExtra();
    $packages = $rootExtra['local-develop-packages'];

    foreach ($packages as $packageString => $packageVersion) {
      $package = $repositoryManager->findPackage($packageString, $packageVersion);
      if ($package) {
        $installPath = self::getInstallPath($package, $composer);

        if (!$fs->exists($installPath)) {
          $repository = $package->getRepository();
          if ($gitDriver = $repository->getDriver()) {
            $gitDriver = $repository->getDriver();
            $repositoryUrl = $gitDriver->getUrl();
            $branchOption = (0 === strpos($packageVersion, 'dev-')) ? '-b ' . substr($packageVersion, strlen('dev-')) . ' ' : '';
            exec('git clone  ' . $branchOption . $repositoryUrl . ' ' . $installPath);
            $io->write('Cloning repository: ' . $packageString);
          }
        }
      }
    }
  }

  /**
   * Reset local repositories to the default branch.
   *
   * @param \Composer\Script\Event $event
   *   The script event.
   */
  public static function resetLocalRepositories(Event $event) {
    $io = $event->getIO();
    $repositoriesInfo = self::getLocalRepositoriesInfo($event);
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $composerRoot = $drupalFinder->getComposerRoot();

    $io->write(PHP_EOL);
    foreach ($repositoriesInfo as $key => $info) {
      $gitCommand = 'git -C ' . $composerRoot . '/' . $info['install_path'];

      $localBranch = trim(shell_exec($gitCommand . ' rev-parse --abbrev-ref HEAD'));

      exec($gitCommand . ' fetch --quiet');
      $gitStatus = shell_exec($gitCommand . ' status --porcelain');
      if (!empty($gitStatus)) {
        $io->write('Stash local changes in ' . $info['package'] . ':' . $localBranch, TRUE);
        exec($gitCommand . ' stash --include-untracked');
      }

      if ($localBranch !== $info['branch']) {
        $io->write('Checkout ' . $info['package'] . ':' . $info['branch'], TRUE);
        exec($gitCommand . ' checkout --quiet ' . $info['branch']);
      }

      $io->write('Merge remote changes into ' . $info['package'] . ':' . $info['branch'], TRUE);
      exec($gitCommand . ' merge --quiet');
    }
  }

  /**
   * Collect information about local repositories.
   *
   * Retrieve available information about the repositories defined in the
   * local-develop-packages key of the composer.json.
   *
   * @param \Composer\Script\Event $event
   *   The script event.
   *
   * @return array
   *   The collected repositories.
   */
  protected static function getLocalRepositoriesInfo(Event $event) {
    $repositoriesInfo = [];
    $composer = $event->getComposer();
    $rootExtra = $composer->getPackage()->getExtra();
    $packages = $rootExtra['local-develop-packages'];

    foreach ($packages as $packageString => $packageVersion) {
      $package = $composer->getRepositoryManager()->findPackage($packageString, $packageVersion);
      if (!$package) {
        continue;
      }

      $repository = $package->getRepository();
      if ($gitDriver = $repository->getDriver()) {
        $info = [];
        $info['package'] = $packageString;
        $info['install_path'] = self::getInstallPath($package, $composer);
        $info['url'] = $gitDriver->getUrl();
        $info['branch'] = (0 === strpos($packageVersion, 'dev-')) ? substr($packageVersion, strlen('dev-')) : '';
        $repositoriesInfo[] = $info;
      }
    }
    return $repositoriesInfo;
  }

  /**
   * Return the install path based on package type.
   *
   * @param \Composer\Package\PackageInterface $package
   * @param \Composer\Composer $composer
   *
   * @return bool|string
   */
  protected static function getInstallPath(PackageInterface $package, Composer $composer) {
    $type = $package->getType();

    $prettyName = $package->getPrettyName();
    if (strpos($prettyName, '/') !== false) {
      list($vendor, $name) = explode('/', $prettyName);
    } else {
      $vendor = '';
      $name = $prettyName;
    }

    $availableVars = compact('name', 'vendor', 'type');

    $extra = $package->getExtra();
    if (!empty($extra['installer-name'])) {
      $availableVars['name'] = $extra['installer-name'];
    }

    if ($composer->getPackage()) {
      $extra = $composer->getPackage()->getExtra();
      if (!empty($extra['installer-paths'])) {
        $customPath = self::mapCustomInstallPaths($extra['installer-paths'], $prettyName, $type, $vendor);
        if ($customPath !== false) {
          return self::templatePath($customPath, $availableVars);
        }
      }
    }

    return false;
  }


  /**
   * Search through a passed paths array for a custom install path.
   *
   * @param  array  $paths
   * @param  string $name
   * @param  string $type
   * @param  string $vendor = NULL
   * @return string
   */
  protected static function mapCustomInstallPaths(array $paths, $name, $type, $vendor = NULL) {
    foreach ($paths as $path => $names) {
      if (in_array($name, $names) || in_array('type:' . $type, $names) || in_array('vendor:' . $vendor, $names)) {
        return $path;
      }
    }

    return false;
  }

  /**
   * Replace vars in a path.
   *
   * @param  string $path
   * @param  array  $vars
   * @return string
   */
  protected static function templatePath($path, array $vars = array()) {
    if (strpos($path, '{') !== false) {
      extract($vars);
      preg_match_all('@\{\$([A-Za-z0-9_]*)\}@i', $path, $matches);
      if (!empty($matches[1])) {
        foreach ($matches[1] as $var) {
          $path = str_replace('{$' . $var . '}', $$var, $path);
        }
      }
    }

    return $path;
  }

}

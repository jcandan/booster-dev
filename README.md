# Booster Dev

A development-friendly repository for Booster, a developer portal Drupal distribution. It allows the developer to fire up a Lando instance of the project, and composer treats the distribution as a git submodule.